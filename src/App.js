import React from 'react';
import {BrowserRouter as Router, Route, Redirect, Switch} from 'react-router-dom'

import NavBar from './components/NavBar'
import Home from './views/Home'
import About from './views/About'
import TopcList from './views/TopicList'
import TopicDetail from './components/TopicDetail'
import NoMatch from './views/NoMatch'

class App extends React.Component {
  render() {
    return (
      <Router>
        <NavBar/>
        <Switch>
          <Route exact path="/Home" component={Home}/>
          <Route exact path="/">
            <Redirect to="/Home"/>
          </Route>
          <Route exact path="/About" component={About}/>
          <Route exact path="/TopcList" component={TopcList}/>
          <Route path="/TopcList/:id" component={TopicDetail}/>
          <Route component={NoMatch}/>
        </Switch>
      </Router>
    );
  }
  
}

export default App;
