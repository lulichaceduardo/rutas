import React, { Component } from 'react';
import {Link} from 'react-router-dom'
class TopicDetail extends Component {
  render() {
    const {id} = this.props.match.params
    return (
      <div>
        <h1>Estas en la subruta con id {id}</h1>
        {/* <Link to="/TopcList">vamos a Topic List</Link> */}
        {/* <button onClick={()=> this.props.history.goBack()}>Volver al topic list</button> */}
        <button onClick={()=> this.props.history.push('/Home')}>Vamos al Home</button>
      </div>
    );
  }
}

export default TopicDetail;