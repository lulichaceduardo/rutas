import React, { Component } from 'react';
import {Link} from 'react-router-dom'
class NavBar extends Component {
  render() {
    return (
      <nav>
        <ul>
          <li><Link to="/Home">Home</Link></li>
          <li><Link to="/About">About</Link></li>
          <li><Link to="/TopcList">TopicList</Link></li>
        </ul>
      </nav>
    );
  }
}

export default NavBar;