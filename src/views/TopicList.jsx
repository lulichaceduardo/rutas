import React, { Component } from 'react';
import {Link} from 'react-router-dom'
class TopicList extends Component {
  render() {
    return (
      <div>
        <h1>Pagina TopicList</h1>
        <ul>
          <li><Link to="/TopcList/topic1">TOPICO 1</Link></li>
          <li><Link to="/TopcList/topic2">TOPICO 2</Link></li>
          <li><Link to="/TopcList/topic3">TOPICO 3</Link></li>
        </ul>
        
      </div>
    );
  }
}

export default TopicList;